// pablo version
// small range(smaller than 1 step range)

@ MOUNTCYLINDER case1

eps: 3.0

expansion: 8517

time: 2.198

cost: 972

setEnvStartGoal(env,
        9.245, 3.42, 0.2, 1.57,
        0.036,  0.0, 0.0,
        3.33, 3.376, 0.33, 0.0,
        0.036,  0.0, 0.0,
        start_id, goal_id);

solution found
