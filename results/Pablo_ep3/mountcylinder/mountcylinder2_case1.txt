// pablo version
// small range(smaller than 1 step range)

@ MOUNTCYLINDER2 case1

eps: 3.0

expansion: 1045

time: 0.298

cost: 1127

setEnvStartGoal(env,
        1.874, 0.5772, 0.1, 0.0,
        0.036,  0.0, 0.0,
        7.365, 4.149, 1.53, 0.0, 
        0.036,  0.0, 0.0,
        start_id, goal_id);

solution found

