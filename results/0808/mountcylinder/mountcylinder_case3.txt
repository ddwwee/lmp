// pablo version
// small range(smaller than 1 step range)

@ MOUNTCYLINDER case3

eps: 1.0

expansion: 100482

time: 50.059s

cost: 1134

setEnvStartGoal(env,
        9.245, 3.42, 0.2, 1.57,
        0.036,  0.0, 0.0,
        1.87,  4.076, 0.520, 0.0,
        0.036,  0.0, 0.0,
        start_id, goal_id);

solution found

